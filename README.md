## Gamification 🏆 

Gamification 🎮 is the design process in which you apply techniques used
commonly in games 🎖  to some other field, which was not initially a game 📱 .
Normally you do that because you want to get some well-known benefits
from games, among others getting players motivated 🥇  and interacting with
your process, application, or whatever you're gamifying ©️ ™️ .