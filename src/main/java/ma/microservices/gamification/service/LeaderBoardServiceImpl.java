package ma.microservices.gamification.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ma.microservices.gamification.domain.LeaderBoardRow;
import ma.microservices.gamification.repository.ScoreCardRepository;

@Service
class LeaderBoardServiceImpl implements LeaderBoardService {

    private ScoreCardRepository scoreCardRepository;

    LeaderBoardServiceImpl(ScoreCardRepository scoreCardRepository) {
        this.scoreCardRepository = scoreCardRepository;
    }

    @Override
    public List<LeaderBoardRow> getCurrentLeaderBoard() {
        return scoreCardRepository.findFirst10();
    }
}