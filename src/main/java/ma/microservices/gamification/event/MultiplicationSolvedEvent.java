package ma.microservices.gamification.event;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Event received when a multiplication has been solved in the system. Provides
 * some context information about the multiplication.
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class MultiplicationSolvedEvent implements Serializable {

	private Long multiplicationResultAttemptId;
	private Long userId;
	private boolean correct;
}